module Matchmaking

  class RoomHandler < RubyPitaya::HandlerBase

    non_authenticated_actions :authenticate

    def authenticate
      room_id = @params[:roomId]

      room = Room.find_by_id(room_id)
      return response_error("Invalid room id") if room.nil?

      @session.uid = room_id
      bind_session_response = @postman.bind_session(@session)

      unless bind_session_response.dig(:error, :code).nil?
        return response = {
          code: RubyPitaya::StatusCodes::CODE_AUTHENTICATION_ERROR,
          msg: 'Error to authenticate',
        }
      end

      response = {
        code: StatusCodes::CODE_OK,
      }
    end

    def roomReady
      room_id = @session.uid

      room = Room.find_by_id(room_id)

      payload = {
        data: room.to_hash(with_user_ids: false),
      }

      route_room_ready = @setup['matchmaking.route.roomReady']
      room.user_ids.each do |room_user_id|
        @postman.push_to_user(room_user_id, route_room_ready, payload)
      end

      response = {
        code: StatusCodes::CODE_OK,
      }
    end

    def getRoomData
      room_id = @session.uid

      room = Room.find_by_id(room_id)
      room_match = get_room_match(room)

      response = {
        code: StatusCodes::CODE_OK,
        data: room_match
      }
    end

    private

    def get_room_match(room)
      match_config = @bll[:matchmaking_data_getter].get_match_config
      player_infos = @bll[:matchmaking_data_getter].get_player_infos(room.user_ids)

      room_match = {
        serverHost: room.host,
        serverPort: room.port,
        numberOfClients: room.user_ids.size,
        matchConfig: match_config,
        playerInfos: player_infos,
      }
    end

    def response_error(message)
      response = {
        code: StatusCodes::CODE_ERROR,
        msg: message,
      }
    end
  end
end