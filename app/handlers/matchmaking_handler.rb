require 'securerandom'

module Matchmaking

  class MatchmakingHandler < RubyPitaya::HandlerBase

    def joinMatch
      user_id = @session.uid
      key_wait_list = @setup['matchmaking.redis.keyWaitList']

      n_players_on_queue = @redis.llen(key_wait_list)
      @redis.rpush(key_wait_list, user_id)

      start_wait_for_create_room if n_players_on_queue == 0

      response = {
        code: StatusCodes::CODE_OK,
      }
    end

    def stopJoinMatch
      user_id = @session.uid
      key_wait_list = @setup['matchmaking.redis.keyWaitList']

      @redis.lrem(key_wait_list, 1, user_id)

      response = {
        code: StatusCodes::CODE_OK,
      }
    end

    private

    def start_wait_for_create_room
      Thread.new do
        key_wait_list = @setup['matchmaking.redis.keyWaitList']
        min_players = @config['matchmaking']['minPlayers']
        max_players = @config['matchmaking']['maxPlayers']
        wait_match_timeout = @config['matchmaking']['waitMatchTimeout']

        sleep(wait_match_timeout)

        user_ids = @redis.lrange(key_wait_list, 0, max_players - 1)
        @redis.ltrim(key_wait_list, max_players, -1)

        n_players_on_queue = @redis.llen(key_wait_list)
        start_wait_for_create_room if n_players_on_queue > 0

        if user_ids.size >= min_players
          create_room(user_ids)
        else
          notify_match_found_timeout(user_ids)
        end
      end
    end

    def create_room(room_user_ids)
      route_match_found = @setup['matchmaking.route.matchFound']

      room = @bll[:matchmaking_room].create_room(room_user_ids)

      payload = {
        data: room.to_hash,
      }

      room_user_ids.each do |room_user_id|
        @postman.push_to_user(room_user_id, route_match_found, payload)
      end
    end

    def notify_match_found_timeout(user_ids)
      routeMatchFoundTimeout = @setup['matchmaking.route.matchFoundTimeout']

      user_ids.each do |user_id|
        @postman.push_to_user(user_id, routeMatchFoundTimeout, {})
      end
    end

    def response_error(code, message)
      response = {
        code: code,
        msg: message,
      }
    end

    def self.setup_authentication_route_if_is_on_development_environment
      if ENV['RUBYPITAYA_ENV'] == 'development'
        non_authenticated_actions :authenticate
        def authenticate
          user_id = @params[:userId] || SecureRandom.uuid

          @session.uid = user_id
          bind_session_response = @postman.bind_session(@session)

          unless bind_session_response.dig(:error, :code).nil?
            puts
            puts "Authentication ERROR: #{bind_session_response}"
            puts
          end

          response = {
            code: StatusCodes::CODE_OK,
          }
        end
      end
    end

    setup_authentication_route_if_is_on_development_environment
  end
end

