module Matchmaking

  class DockerRoomBLL

    def initialize(redis, setup)
      @redis = redis
      @setup = setup
      @initial_port = @setup['matchmaking.roomBll.docker.initialPort']
      @final_port = @setup['matchmaking.roomBll.docker.finalPort']
      @container_image = @setup['matchmaking.roomBll.docker.containerImage']
      @container_port = @setup['matchmaking.roomBll.docker.containerPort']

      @current_port = 0
    end

    def create_room(user_ids)
      use_next_port
      set_current_port(@current_port)

      host = "localhost"

      room = Room.new(container_id: 'empty_id', host: host, port: @current_port, user_ids: user_ids)
      room.save

      puts "docker run -d -e ROOM_ID=#{room.id} -p #{@current_port}:#{@current_port} #{@container_image}"

      id = `docker run -d -e ROOM_ID=#{room.id} -p #{@current_port}:#{@current_port} #{@container_image}`

      room.container_id = id
      room.save

      room
    end

    def destroy_room(room_id)
      room = Room.find_by_id(room_id)

      `docker rm -f #{container_id}`

      room.save
      room
    end

    private

    def get_current_port
      key_current_port = @setup['matchmaking.roomBll.docker.keyCurrentPort']

      return @initial_port.to_i unless @redis.exists(key_current_port)
      @redis.get(key_current_port).to_i
    end

    def set_current_port(port)
      key_current_port = @setup['matchmaking.roomBll.docker.keyCurrentPort']
      
      @redis.set(key_current_port, port)
    end

    def get_next_port
      port = get_current_port
      return @initial_port if port == @final_port
      port + 1
    end

    def use_next_port
      key_current_port = @setup['matchmaking.roomBll.docker.keyCurrentPort']
      
      @redis.watch(key_current_port) do
        @current_port = get_next_port

        @redis.multi do
          set_current_port = @current_port
        end
      end

      @current_port
    end
  end
end