require 'net/http'

module Matchmaking

  class LocalRoomBLL

    def initialize(setup)
      @setup = setup
    end

    def create_room(user_ids)
      hostname = @setup['matchmaking.roomBll.local.hostname']
      game_server_url = @setup['matchmaking.roomBll.local.gameServerUrl']
      game_server_port = @setup['matchmaking.roomBll.local.gameServerPort']

      room = Room.new(container_id: 'empty_id',
                      host: hostname,
                      port: game_server_port,
                      user_ids: user_ids)
      room.save

      send_roomid_to_gameserver(room.id)

      room
    end

    def destroy_room(room_id)
      room = Room.find_by_id(room_id)

      room.save
      room
    end

    private

    def send_roomid_to_gameserver(room_id)
      game_server_url = @setup['matchmaking.roomBll.local.gameServerUrl']
      game_server_port = @setup['matchmaking.roomBll.local.gameServerPort']

      puts
      puts
      puts "http://#{game_server_url}:#{game_server_port}?roomId=#{room_id}"
      puts
      puts

      uri = URI("http://#{game_server_url}:#{game_server_port}?roomId=#{room_id}")
      Net::HTTP.get(uri)
    end
  end
end
