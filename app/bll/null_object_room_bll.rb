module Matchmaking

  class NullObjectRoomBLL

    def initialize
    end

    def create_room(user_ids)
      room = Room.new(
        container_id: 'empty_id', 
        host: 'any_host', 
        port: 0000, 
        user_ids: user_ids
      )

      puts ''
      puts 'ROOM CREATED!'
      puts ''

      room
    end

    def destroy_room(room_id)
      room = Room.new

      puts ''
      puts 'ROOM DELETED!'
      puts ''

      room
    end
  end
end