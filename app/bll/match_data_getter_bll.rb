module Matchmaking

  class MatchDataGetterBLL

    def get_match_config
      match_config = {
        maxNumberOfPlayers: 8,
        matchDurationSeconds: 20,
      }
    end

    def get_player_infos(user_ids)
      user_ids.map do |user_id|
        player_info = {
          userId: user_id
        }
      end
    end
  end
end