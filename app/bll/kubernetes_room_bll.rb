require 'erb'
require 'ostruct'

module Matchmaking

  class KubernetesRoomBLL

    KUBERNETES_GAMEPLAY_TEMPLATE_FILE = File.join(__dir__, '../../kubernetes/gameplay/gameplay-template.yaml')

    def initialize(redis, setup)
      @redis = redis
      @setup = setup

      @initial_port = @setup['matchmaking.roomBll.kubernetes.initialPort'].to_i
      @final_port = @setup['matchmaking.roomBll.kubernetes.finalPort'].to_i
      @container_image = @setup['matchmaking.roomBll.kubernetes.containerImage']
      @service_host = @setup['matchmaking.roomBll.kubernetes.serviceHost']

      @current_port = 0
    end

    def create_room(user_ids)
      use_next_port
      set_current_port(@current_port)

      room = Room.new(container_id: 'empty_id', host: @service_host, port: @current_port, user_ids: user_ids)
      room.save

      namespace = @setup['matchmaking.roomBll.kubernetes.namespace']
      room_name_prefix = @setup['matchmaking.roomBll.kubernetes.roomNamePrefix']
      seconds_to_destroy_gameserver = @setup['matchmaking.roomBll.kubernetes.secondsToDestroyGameServer']

      template_struct = OpenStruct.new(
        name: "#{room_name_prefix}-#{@current_port}",
        port: @current_port,
        container_image: @container_image,
        room_id: room.id
      )

      template = File.open(KUBERNETES_GAMEPLAY_TEMPLATE_FILE, &:read)
      template_result = ERB.new(template).result(template_struct.instance_eval { binding })

      File.open('/root/temp_template.yaml', 'w') { |f| f.write(template_result) }
      puts "kubectl -n #{namespace} apply -f /root/temp_template.yaml"
      output = `kubectl -n #{namespace} apply -f /root/temp_template.yaml`
      File.delete('/root/temp_template.yaml')

      room_name = "#{room_name_prefix}-#{@current_port}"
      fork { sleep(seconds_to_destroy_gameserver); exec("kubectl -n #{namespace} delete deployment #{room_name}") }

      next_room_name = "#{room_name_prefix}-#{get_next_port}"
      puts "kubectl -n #{namespace} delete deployment #{next_room_name}"
      fork { exec("kubectl -n #{namespace} delete deployment #{next_room_name}") }

      room.container_id = template_struct.name
      room.save

      room
    end

    def destroy_room(room_id)
    end

    private

    def get_current_port
      key_current_port = @setup['matchmaking.roomBll.kubernetes.keyCurrentPort']

      return @initial_port.to_i unless @redis.exists(key_current_port)
      @redis.get(key_current_port).to_i
    end

    def set_current_port(port)
      key_current_port = @setup['matchmaking.roomBll.kubernetes.keyCurrentPort']

      @redis.set(key_current_port, port)
    end

    def get_next_port
      port = get_current_port
      return @initial_port if port >= @final_port
      port + 1
    end

    def use_next_port
      key_current_port = @setup['matchmaking.roomBll.kubernetes.keyCurrentPort']

      @redis.watch(key_current_port) do
        @current_port = get_next_port

        @redis.multi do
          set_current_port = @current_port
        end
      end

      @current_port
    end
  end
end