module Matchmaking

  class AppInitializer < RubyPitaya::InitializerBase

    # method:     run
    # parameter:  initializer_content
    # attributes:
    #  - bll
    #    - class: RubyPitaya::InstanceHolder
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/instance_holder.rb
    #    - methods:
    #      - add_instance(key, instance)
    #        - add any instance to any key
    #      - [](key)
    #        - get instance by key
    #  - redis
    #    - link: https://github.com/redis/redis-rb/
    #  - config
    #    - class: RubyPitaya::Config
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/config.rb
    #    - methods:
    #      - [](key)
    #        - get config file by config path
    #  - setup
    #    - class: RubyPitaya::Setup
    #    - link: https://gitlab.com/LucianoPC/ruby-pitaya/-/blob/master/lib/rubypitaya/core/setup.rb
    #    - methods:
    #      - [](key)
    #        - get config file by config path

    def run(initializer_content)
      bll = initializer_content.bll
      redis = initializer_content.redis
      setup = initializer_content.setup

      room_bll = create_room_bll(redis, setup)
      data_getter_bll = MatchDataGetterBLL.new

      bll.add_instance(:matchmaking_room, room_bll)
      bll.add_instance(:matchmaking_data_getter, data_getter_bll)
    end

    private

    def create_room_bll(redis, setup)
      currentRoomBll = setup['matchmaking.currentRoomBll']

      roomBll = NullObjectRoomBLL.new
      if currentRoomBll == 'local'
        roomBll = LocalRoomBLL.new(setup)
      elsif currentRoomBll == 'docker'
        roomBll = DockerRoomBLL.new(redis, setup)
      elsif currentRoomBll == 'kubernetes'
        roomBll = KubernetesRoomBLL.new(redis, setup)
      end

      roomBll
    end
  end
end