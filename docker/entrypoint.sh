#!/bin/bash

cmd="$@"

echo "=> Waiting for postgres"
until PGPASSWORD=$DATABASE_PASSWORD psql -h "$DATABASE_HOST" -U "$DATABASE_USER" -c '\q' > /dev/null 2>&1; do
  echo "=> Waiting for Postgres..."
  sleep 1
done

echo "=> Postgres is ready"

exec $cmd
